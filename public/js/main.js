$(document).ready(function () {




    //Todo Validate pass innew user
    $('.passUser ,#rememberPass').keyup(function () {
        if (($('.passUser').val() === $('#rememberPass').val()) && $('.passUser').val().length >= 7 && $('#rememberPass').val().length >= 7) {
            $('#btn-save-user').prop("disabled", false);
            $('#rememberPass').css('border', ' 2px solid #A8EE35');
            $('.passUser').css('border', ' 2px solid #A8EE35');
            $('#info-user').empty();
        } else if ($('.passUser').val() === "" || $('#rememberPass').val() === "") {
            $('#rememberPass').attr('placeholder', 'Repita contraseña');
            $('.passUser').attr('placeholder', 'Contraseña');
            $('#rememberPass').css('border', ' 1px solid transparent');
            $('.passUser').css('border', ' 1px solid transparent');
        } else if ($('.passUser').val().length < 7 || $('#rememberPass').val().length < 7) {
            $('#rememberPass').css('border', ' 2px solid red');
            $('.passUser').css('border', ' 2px solid red');
            $('#info-user').html('<br><h6 class="mt-3" style="color:orange"> Mínimo 8 caracteres alfanuméricos....</h6>');
        } else {
            $('#rememberPass').css('border', ' 2px solid red');
            $('.passUser').css('border', ' 2px solid red');
            $('#btn-save-user').prop("disabled", true);
        }
    });



    //TODO: new user added
    $('form[name="user"]').submit(function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var formSerialize = form.serialize();
        $.ajax({
            type: 'POST',
            url:Routing.generate('ajax.user'),
            data: formSerialize,
            async: true,
            success: function (data, status, object) {
           },
            error: function (data, status, object) {
            }
        });
    });


});



